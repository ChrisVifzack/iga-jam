using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* GameState as singleton*/
public class GameState
{

    private static readonly GameState _instance = new GameState();
    public static GameState Instance { get => _instance; }

    public int currentScore { get; set; }
    public bool GameEnded { get; set; }
    public bool IsGoodEnding { get; set; }

    public GameState()
    {
        GameEnded = false;
        currentScore = 0;
    }

    public void Reset()
    {
        GameEnded = false;
        currentScore = 0;
    }
}
