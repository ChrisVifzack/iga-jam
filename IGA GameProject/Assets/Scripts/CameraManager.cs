using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public GameObject[] cameras;
    public float[] switchToNextCameraTime;
    
    private bool startTimer = false;
    private int currentCameraIndex = 0;

    void Start()
    {
        startTimer = true;

        foreach (GameObject camera in cameras)
        {
            camera.SetActive(false);

        }
        cameras[currentCameraIndex].SetActive(true);
    }

    void Update()
    {
        if (currentCameraIndex >= cameras.Length - 1 || currentCameraIndex >= switchToNextCameraTime.Length)
        {
            return;
        }

        if (startTimer)
        {
            switchToNextCameraTime[currentCameraIndex] -= Time.deltaTime;

            if (switchToNextCameraTime[currentCameraIndex] <= 0.0f)
            {
                cameras[currentCameraIndex].SetActive(false);
                currentCameraIndex++;
                cameras[currentCameraIndex].SetActive(true);                
            }                       
        }
    }
}
