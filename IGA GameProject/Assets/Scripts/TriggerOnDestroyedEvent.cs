using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerOnDestroyedEvent : MonoBehaviour
{
    public List<GameEvent> DestroyedEvent;
    public bool IsPunched = false;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RaiseDestroyedEvent(int index)
    {
        if (index < DestroyedEvent.Count) {
            DestroyedEvent[index].Raise();
        }
    }

    private void OnDestroy()
    {
        
    }
}
