using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public AudioSource GlassSound;
    public AudioSource GlassStashSound;
    public AudioSource RadioactiveSound;
    public AudioSource RadioactiveFloorSound;
    public int NumGoodEnding = 18;

    private void Awake()
    {
        GameState.Instance.Reset();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void increaseScore()
    {
        GameState.Instance.currentScore++;
    }

    public void decreaseScore()
    {
        GameState.Instance.currentScore--;
    }

    private void playSound(AudioSource audio)
    {
        audio.Play();
    }

    public void playGlassSound()
    {
        playSound(GlassSound);
    }

    public void playGlassStashedSound()
    {
        playSound(GlassStashSound);
    }

    public void playRadioactiveSound()
    {
        playSound(RadioactiveSound);
    }

    public void playRadioactiveFloorSound()
    {
        playSound(RadioactiveFloorSound);
    }

    public void EndGame()
    {
        playSound(RadioactiveSound);
        GameState.Instance.IsGoodEnding = GameState.Instance.currentScore >= NumGoodEnding;
        GameState.Instance.GameEnded = true;

        if (GameState.Instance.IsGoodEnding) {
            Invoke("LoadGoodEnding", 6.45f);
        }
        else {
            Invoke("LoadBadEnding", 6.45f);
        }
    }

    private void LoadGoodEnding()
    {
        SceneManager.LoadScene("GoodEnding");
    }

    private void LoadBadEnding()
    {
        SceneManager.LoadScene("BadEnding");
    }

}
