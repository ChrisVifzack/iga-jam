using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    public GameObject CorrectTrash;
    public int sourceIndex = 0;

    private System.Type CorrectTrashType;

    // Start is called before the first frame update
    void Start()
    {
        if(CorrectTrash != null) {
            CorrectTrashType = CorrectTrash.GetType();
        } else {
            CorrectTrashType = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "Goo" || other.tag == "Glass") {
            TriggerOnDestroyedEvent component = other.gameObject.GetComponent<TriggerOnDestroyedEvent>();
            if(component != null) {
                component.RaiseDestroyedEvent(sourceIndex);
            }
            component.RaiseDestroyedEvent(sourceIndex);
            
            Destroy(other.gameObject);
        }
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Goo" || collision.gameObject.tag == "Glass") {
            TriggerOnDestroyedEvent component = collision.gameObject.GetComponent<TriggerOnDestroyedEvent>();
            if (component != null) {
                component.RaiseDestroyedEvent(sourceIndex);
            }
            Destroy(collision.gameObject.gameObject);
        }
    }
}
