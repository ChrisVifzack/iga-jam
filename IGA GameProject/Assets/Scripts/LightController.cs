using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{
    public float[] timeTillNextTotalDarkness = { 10f, 10f, 10f };
 
    private int index = 0;
    private float timer = 0f;

    private Light directionalLight;
    private Animator animator;


    void Start()
    {
        directionalLight = GetComponent<Light>();
        animator = GetComponent<Animator>();
        timer = timeTillNextTotalDarkness[index++];
    }

    void Update()
    {
        if (!directionalLight || !animator || index >= timeTillNextTotalDarkness.Length)
        {
            return;
        }

        timer -= Time.deltaTime;
        if (timer <= 0f)
        {   
            timer = timeTillNextTotalDarkness[index++];
            animator.SetTrigger("CompleteDarknessTrigger");            
        }     
    }
}
