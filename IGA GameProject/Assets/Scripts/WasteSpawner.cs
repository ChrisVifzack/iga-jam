using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WasteSpawner : MonoBehaviour
{

    public GameObject Prefab1;

    public GameObject Prefab2;

    public GameObject Prefab3;

    private int[] WasteList = { 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 1, 2 , 3};

    private int WasteListIndex = 0;

    public float SpawnTime = 0f;
    public float secondsBetweenSpawn;

    void Start()
    {
    }

    void Update()
    {
        if (WasteList.Length <= WasteListIndex)
        {
            return;
        }

        SpawnTime += Time.deltaTime;

        if (SpawnTime > secondsBetweenSpawn)
        {
            SpawnTime = 0;
            GameObject ObjectToSpawn;
            if (WasteList[WasteListIndex] == 1)
            {

                ObjectToSpawn = Prefab1;
            }
            else if (WasteList[WasteListIndex] == 2)
            {
                ObjectToSpawn = Prefab2;
            }
            else
            {
                ObjectToSpawn = Prefab3;
            }
            GameObject NewObject = Instantiate(ObjectToSpawn, transform.position, transform.rotation, transform) as GameObject;
            WasteListIndex++;
        }
    }
}
