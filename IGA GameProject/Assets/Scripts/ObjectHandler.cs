using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHandler : MonoBehaviour
{
    private GameObject TrashInTrigger;

    // Start is called before the first frame update
    void Start()
    {
        TrashInTrigger = null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        TrashInTrigger = other.gameObject;
    }

    private void OnTriggerExit(Collider other)
    {
        TrashInTrigger = null;
    }

    public GameObject GetTrashInTrigger()
    {
        return TrashInTrigger;
    }
}
