using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Animator animator;
    public GameObject HitBox;
    public float leftArmStrength = 11;
    public float rightArmStrength = 22;

    public GameObject correctTrashLeft;
    public GameObject correctTrashRight;

    public GameEvent ScoreIncreaseEvent;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameState.Instance.GameEnded && animator != null) {
            if (GameState.Instance.IsGoodEnding) {
                animator.SetTrigger("GoodEndingTrigger");
            } else {
                animator.SetTrigger("BadEndingTrigger");
            }
            this.enabled = false;
        }
    }

    public void OnThrowLeft()
    {
        if (animator != null && animator.GetCurrentAnimatorStateInfo(0).IsName("Idle")) {

            GameObject Trash = HitBox.GetComponent<ObjectHandler>().GetTrashInTrigger();
            if (Trash == null) {
                animator.SetTrigger("ThrowLeftTrigger");
            }
            else if (correctTrashLeft != null && correctTrashLeft.tag.Equals(Trash.tag)) {
                animator.SetTrigger("ThrowLeftTrigger");

                TriggerOnDestroyedEvent component = Trash.GetComponent<TriggerOnDestroyedEvent>();
                if (component != null && !component.IsPunched) {
                    Trash.GetComponent<Rigidbody>().AddForce(-rightArmStrength, 2 * rightArmStrength, 0, ForceMode.Impulse);
                    ScoreIncreaseEvent.Raise();
                    component.IsPunched = true;
                }

                
            }
            else {
                TriggerOnDestroyedEvent component = Trash.GetComponent<TriggerOnDestroyedEvent>();
                if (component != null) {
                    component.RaiseDestroyedEvent(1);
                }
                component.RaiseDestroyedEvent(1);

                animator.SetTrigger("WrongLeftTrigger");
                Destroy(Trash);
            }
        }
    }

    public void OnThrowRight()
    {
        if (animator != null && animator.GetCurrentAnimatorStateInfo(0).IsName("Idle")) {
            
            GameObject Trash = HitBox.GetComponent<ObjectHandler>().GetTrashInTrigger();
            if (Trash == null) {
                animator.SetTrigger("ThrowRightTrigger");
            }
            else if(correctTrashLeft != null && correctTrashRight.tag.Equals(Trash.tag)) {
                animator.SetTrigger("ThrowRightTrigger");

                TriggerOnDestroyedEvent component = Trash.GetComponent<TriggerOnDestroyedEvent>();
                if (component != null && !component.IsPunched) {
                    Trash.GetComponent<Rigidbody>().AddForce(leftArmStrength, 2 * leftArmStrength, 0, ForceMode.Impulse);
                    ScoreIncreaseEvent.Raise();
                    component.IsPunched = true;
                }

            }
            else {
                TriggerOnDestroyedEvent component = Trash.GetComponent<TriggerOnDestroyedEvent>();
                if (component != null) {
                    component.RaiseDestroyedEvent(1);
                }
                component.RaiseDestroyedEvent(1);

                animator.SetTrigger("WrongRightTrigger");
                Destroy(Trash);
            }
        }
    }
}
