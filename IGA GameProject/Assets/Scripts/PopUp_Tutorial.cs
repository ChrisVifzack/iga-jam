using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUp_Tutorial : MonoBehaviour
{
    public GameObject tutorialCanvas;
    private float time = 0f;
    public float secondsBeforeInactive;

void Start()
{

}
    void Update()
    {
        time += Time.deltaTime;

        if(gameObject.activeSelf)
        {
            tutorialCanvas.SetActive(true);

        }

        if(time > secondsBeforeInactive)
        {
            tutorialCanvas.SetActive(false);
        }

    }
}
