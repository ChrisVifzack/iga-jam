﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventTrigger : MonoBehaviour
{
    public GameEvent gameEvent;

    public void TriggerEvent()
    {
        gameEvent.Raise();
    }
}
