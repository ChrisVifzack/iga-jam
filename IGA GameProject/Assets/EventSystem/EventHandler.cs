﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/**
 * Listens to a certain event and handles it when it is Raised.
 */
public class EventHandler : MonoBehaviour
{
    // The event that the EventHandler is listening to (a "event type condition" so to speak)
    public GameEvent Event;

    // Response is the action that is taken if the condition is fullfilled
    public UnityEvent Response;

    void OnEnable()
    {
        if (Event != null) {
            Event.Register(this);
        }
        else {
            Debug.LogWarning("EventHandler does not have any GameEvent set! (" + gameObject +")");
        }
    }

    void OnDisable()
    {
        if (Event != null) {
            Event.Unregister(this);
        }
    }

    public void OnEventRaised()
    {
        if (Response != null) {
            Response.Invoke();
        }
        else {
            Debug.LogWarning("EventHandler does not have any Response set! (" + gameObject + ")");
        }
    }
}
