﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "GameEvent", menuName = "ScriptableObjects/EventHandling/GameEvent", order = 1)]
public class GameEvent : ScriptableObject
{
    private List<EventHandler> _eventListeners = new List<EventHandler>();

    public float Value;
    public GameObject GameObject;

    public void Raise()
    {
        for (int i = 0; i < _eventListeners.Count; ++i)
        {
            _eventListeners[i].OnEventRaised();
        }
    }

    public void Register(EventHandler listener)
    {
        if (!_eventListeners.Contains(listener))
        {
            _eventListeners.Add(listener);
        }
    }

    public void Unregister(EventHandler listener)
    {
        if (_eventListeners.Contains(listener))
        {
            _eventListeners.Remove(listener);
        }
    }

}

